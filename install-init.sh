#!/bin/bash
# K3op System
# This script will download all required scripts to configure a server

# Load unicorns
source rainbow.sh
# Load a configuration

export K3OP_SYSADMIN_URL="bitbucket.org/k3op/install.git"
export K3OP_SYSADMIN_NAME="install"
export K3OP_HOSTTYPE="web"
export K3OP_HOSTNAME="p1"
export K3OP_DOMAIN="k3op.com"
LANGUAGE=en_US.UTF-8


sudo hostname p1.k3op.com

varecho=$(echocyan "Setup Locale")
echo "$varecho"
# Setup locale
sudo sh -c "echo 'LANG=$LANGUAGE\nLANGUAGE=$LANGUAGE\n' > /etc/default/locale"
sudo sh -c "echo '
export LANGUAGE=$LANGUAGE
export LANG=$LANGUAGE
export LC_ALL=$LANGUAGE
export LC_CTYPE=$LANGUAGE
' >> $HOME/.bashrc"
source $HOME/.bashrc

varecho=$(echocyan "Setup Zshrc")
#export PS1="${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ "
echo "Adding a bit of color and formatting to the command prompt"
echo '
export PS1="${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\H\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ "
' >> $HOME/.bashrc
source $HOME/.bashrc

varecho=$(echocyan "Setup hosts")

# Setup hosts
sudo mv /etc/hosts /etc/hosts.bak
sudo sh -c  "echo '
127.0.0.1       localhost
163.172.154.114      $K3OP_HOSTNAME.$K3OP_DOMAIN     $K3OP_HOSTNAME
::1     ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
ff02::3 ip6-allhosts
' >> /etc/hosts"
