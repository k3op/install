#!/bin/bash

LOG_FILE="$HOME/install-nginx.log"

export NPS_VERSION=1.11.33.2
export NGINX_VERSION=1.11.3

mkdir /var/cache/nginx


mkdir -p /var/lib/nginx
mkdir -p /var/lib/nginx/body
mkdir -p /var/lib/nginx/fastcgi


cd
apt-get -y install libgeoip-dev
sudo apt-get -y install libgd-dev
sudo apt-get -y install build-essential zlib1g-dev libpcre3 libpcre3-dev unzip libssl-dev
sudo apt-get -y install libxml2-dev libxslt1-dev python-dev


wget https://github.com/pagespeed/ngx_pagespeed/archive/release-${NPS_VERSION}-beta.zip -O release-${NPS_VERSION}-beta.zip
unzip release-${NPS_VERSION}-beta.zip
cd ngx_pagespeed-release-${NPS_VERSION}-beta/

wget https://dl.google.com/dl/page-speed/psol/${NPS_VERSION}.tar.gz
tar -xzvf ${NPS_VERSION}.tar.gz  # extracts to psol/


cd
wget http://nginx.org/download/nginx-${NGINX_VERSION}.tar.gz
tar -xvzf nginx-${NGINX_VERSION}.tar.gz
cd nginx-${NGINX_VERSION}/


./configure --prefix=/opt/nginx --sbin-path=/usr/sbin/nginx --modules-path=/usr/lib/nginx/modules --conf-path=/etc/nginx/nginx.conf --http-log-path=/var/log/nginx/access.log --pid-path=/var/run/nginx.pid --lock-path=/var/run/nginx.lock  --http-scgi-temp-path=/var/cache/nginx/scgi_temp --user=nginx --group=nginx --with-http_ssl_module --with-http_realip_module --with-http_addition_module --with-http_sub_module --with-http_dav_module --with-http_flv_module --with-http_mp4_module --with-http_gunzip_module --with-http_gzip_static_module --with-http_random_index_module --with-http_secure_link_module --with-http_stub_status_module --with-http_auth_request_module --with-http_xslt_module=dynamic --with-http_image_filter_module=dynamic --with-threads --with-stream --with-stream_ssl_module --with-http_slice_module --with-mail --with-mail_ssl_module --with-file-aio --with-ipv6 --with-http_v2_module  --add-module=$HOME/ngx_pagespeed-release-${NPS_VERSION}-beta >> $LOG_FILE


sudo make
sudo make install


sudo adduser --system --no-create-home --disabled-login --disabled-password --group nginx


sudo sh -c "echo 'NOMDEDOMAINE' > /etc/hostname"


echo "[Unit]
Description=NGINX
After=syslog.target network.target remote-fs.target nss-lookup.target

[Service]
Type=forking
PIDFile=/run/nginx.pid
ExecStartPre=/usr/sbin/nginx -t -q -g 'daemon on; master_process on;'
ExecStart=/usr/sbin/nginx -g 'daemon on; master_process on;'
ExecReload=/bin/kill -s HUP $MAINPID
ExecStop=/bin/kill -s QUIT $MAINPID
PrivateTmp=true
TimeoutStopSec=5
KillMode=mixed

[Install]
WantedBy=multi-user.target" > /lib/systemd/system/nginx.service

sudo chmod +x /lib/systemd/system/nginx.service

mkdir /etc/nginx/sites-available
mkdir /etc/nginx/sites-enabled
mkdir /var/www

chmod 755 /var/www
sudo chown -R www-data:www-data /var/www

usermod -a -G www-data nginx

cp ~/install/skel/nginx.conf /etc/nginx/nginx.conf


apt-get -y install php7.0-fpm
cp ~/install/skel/php.ini /etc/php/7.0/fpm/php.ini
sudo service php7.0-fpm restart


sudo service nginx start

# Install LOG
#curl -O https://www.loggly.com/install/configure-nginx.sh
#sudo bash configure-nginx.sh -a keop -u k3op
#9H0Drs0RQSeU

sudo git clone https://github.com/letsencrypt/letsencrypt /etc/letsencrypt

sudo openssl dhparam -out /etc/ssl/certs/dhparam.pem 2048

cd /etc/nginx/
mkdir snippets

cp ~/install/skel/ssl-params.conf /etc/nginx/snippets/ssl-params.conf


cd /root
rm -rf release-${NPS_VERSION}-beta.zip
rm -rf ngx_pagespeed-release-${NPS_VERSION}-beta/
rm -rf  nginx-${NGINX_VERSION}.tar.gz
rm -rf  nginx-${NGINX_VERSION}/


echo -e "\e[31mNginx Installed with success\e[0m"

# mkdir /var/www/default
# mkdir /var/www/default/html
# # Copy index file for server
# cp /root/sysadmin-k3op/install/skel/index.server.html /var/www/default/html/index.html
# sudo chown -R www-data:www-data /var/www/default/;

exit;
