#!/bin/bash

source rainbow.sh

email='contact@k3op.com'
wwwDir='/var/www/'
sourceDir='/html'
domain=$1
sitesEnable='/etc/nginx/sites-enabled/'
sitesAvailable='/etc/nginx/sites-available/'


varecho=$(echogreen "Clean directory")
echo "$varecho"

rm -rf $wwwDir$domain
rm -rf $sitesAvailable$domain
rm -rf $sitesEnable$domain
rm -rf /etc/nginx/sites-available/$domain

varecho=$(echogreen "Write Nginx Vhost")
echo "$varecho"

### write NGINX conf
touch $sitesAvailable$domain

varecho=$(echogreen "Setup $domain:80")
echo "$varecho"
echo "
server {

  listen 80;
  server_name www.$domain $domain;

  root /var/www/$domain/html;

  index index.html index.php;

  location / {
    try_files \$uri \$uri/ =404;
  }

  location ~ /.well-known {
        allow all;
    }
 location ~ \.php$ {
        try_files \$uri =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass unix:/var/run/php/php7.0-fpm.sock;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
        include fastcgi_params;
    }

}" > $sitesAvailable$domain

### enable website
sudo ln -s $sitesAvailable$domain $sitesEnable$domain


varecho=$(echogreen "Pull $domain.git")
echo "$varecho"

git clone ssh://git@bitbucket.org/k3op/$domain.git $wwwDir$domain
cd $wwwDir

sudo chown -R www-data:www-data $wwwDir$domain;
chmod -R 755 $wwwDir$domain

varecho=$(echogreen "Nginx Started")
echo "$varecho"


sudo service nginx start



if [[ $(sudo nginx -t && sudo service nginx reload | grep 'successful') = *java* ]]; then
  echo "Found a Tomcat!"
fi

cd /etc/letsencrypt
# sudo ./letsencrypt-auto certonly --agree-tos --text -a webroot --webroot-path=$wwwDir$domain/html --email contact@k3op.com -d $domain -d www.$domain  --staging
#sudo ./letsencrypt-auto certonly --agree-tos --renew-by-default --text -a webroot --webroot-path=/var/www/$domain/html/ --email contact@k3op.com -d $domain
./letsencrypt-auto certonly --agree-tos --text -a webroot --webroot-path=/var/www/$domain/html/ --email contact@k3op.com -d $domain #-d www.$domain

# fix permission
sudo chown -R www-data:www-data $wwwDir$domain;
chmod -R 755 $wwwDir$domain

 echo "ssl_certificate /etc/letsencrypt/live/$domain/fullchain.pem;
 ssl_certificate_key /etc/letsencrypt/live/$domain/privkey.pem;
 " > /etc/nginx/snippets/ssl-$domain.conf

varecho=$(echogreen "Setup $domain:443")
echo "$varecho"
echo "
server {
    listen 80;
    server_name $domain;
    return 301 https://\$host\$request_uri;
}

server {
   listen 443 ssl http2;
   listen [::]:443 ssl http2;
   server_name $domain;

   include /etc/nginx/snippets/ssl-$domain.conf;
   include /etc/nginx/snippets/ssl-params.conf;

  root /var/www/$domain/html;

  index index.html index.php;

  location / {
    try_files \$uri \$uri/ =404;
  }

  location ~ /.well-known {
    allow all;
  }
  location ~ \.php$ {
    try_files \$uri =404;
    fastcgi_split_path_info ^(.+\.php)(/.+)$;
    fastcgi_pass unix:/var/run/php/php7.0-fpm.sock;
    fastcgi_index index.php;
    fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
    include fastcgi_params;
  }

}" > $sitesAvailable$domain


sudo service nginx restart

varecho=$(echogreen "Complete")
echo "$varecho"
echo "https://$domain"

exit;
