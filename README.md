# K3op Scripts

Collection of scripts to setup servers (OSX/ LINUX)




## SSH & CLONE projects

### Enable SSH
Create SSH keys
```
ssh-keygen
```

Copy SSH keys
```
#!shell
ssh-copy-id -f -i contact@k3op.com.pub contact@k3op.com
```


**VPS root (optional)**
To login as a root on vps when username is ubuntu for example, become root with
```
#!shell

sudo passwd root
```



### Enable CLONE

Copy Bitbucket key to project's **ACCESS KEYS**

```
#!shell

cat id_rsa.pub
```


#### Copy scripts


```
#!shell

git clone https://k3op@bitbucket.org/k3op/install.git
```


## Setup server

Once current dir is *install*, you can run the following commands


**Setup Locales and host**

```
./install-init.sh
```


**Setup Server Security and Packages**

```
./install-server.sh
```


**Install Nginx with Page Speed and Lets Encrypt**

```
./install-nginx.sh
```

**Install RoR with RVM**

```
./install-rails.sh
```


**Create Virtual Host with SSL support**

```
./create-host.sh example.com
```

**Create Virtual Host with SSL support**

```
./create-host.sh example.com
```

**Create Backups** (deprecated)

```
./create-backup.sh
```



## Setup Wordpress

```
sudo ee site create example.com --wp
```

