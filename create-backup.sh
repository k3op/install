#!/bin/bash

cp /etc/nginx/sites-available/* /root/sysadmin-k3op/backup/conf/
cp /etc/nginx/nginx.conf /root/sysadmin-k3op/backup/conf/nginx.conf
cp -R /etc/letsencrypt/ /root/sysadmin-k3op/backup/cert/

cd /root/sysadmin-k3op/
git add .
git commit -m "Backup conf"
git push origin master