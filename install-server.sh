#!/bin/bash
# Namecheap A reccord must be set before !
# ssh-copy to install ssh key
# sh install-server.sh master s1
# sh install-server.sh slave p1


source rainbow.sh

echo "######################################"
echo "#### Install Server: k3op systems ####"
echo "######################################"


HOSTNAME=$2
DOMAIN=k3op.com
LOG_FILE="$HOME/install-server.log"
USER="root"
MODE=$1

# if [ $MODE = "master" ]
# then
# echo "Installing serverdensity"
# curl http://archive.serverdensity.com/agent-install.sh | \
# bash -s -- -a k3op -k 58e008510ffd6a69dd3cf0ea91b4cab9

# echo "Installing Newrelic"
# echo 'deb http://apt.newrelic.com/debian/ newrelic non-free' | sudo tee /etc/apt/sources.list.d/newrelic.list
# wget -O- https://download.newrelic.com/548C16BF.gpg | apt-key add -
# apt-get update
# apt-get install newrelic-sysmond
# nrsysmond-config --set license_key=a94307d84e2e4f0f9c9376f42b852384eb53247f
# systemctl start newrelic-sysmond

# elif [ $MODE = "slave" ]
# then
#  	echo "Nothing particular"
# else
#  	echo "Error: Missing mode (master or slave).\n ex: install-server.sh master domain.com"
#  	exit;
# fi

# if [ $HOSTNAME -eq 0 ]
#   then
#  	echo "Error: Missing domain.\n ex: install-server.sh master domain.com"
# fi


sudo apt-get purge runit &>> /dev/null
sudo apt-get autoremove &>> /dev/null
sudo apt -y update &>> /dev/null


git config --global user.name "k3op"
git config --global user.email "contact@k3op.com"

varecho=$(echocyan "Add Prompt Color")
echo "$varecho"



sudo locale-gen "en_US.UTF-8"
#sudo dpkg-reconfigure locales

echo "Clean & Update system"

sudo apt-get -y upgrade

echo "Ensure that sshd starts after eth0 is up, not just after filesystem"
sed -i "s/start on filesystem/start on filesystem and net-device-up IFACE=eth0/g" /etc/init/ssh.conf

echo "Disabling X11 forwarding"
sed -i "s/X11Forwarding yes/X11Forwarding no/g" /etc/ssh/sshd_config

echo "Disabling sshd DNS resolution"
echo "UseDNS no" >> /etc/ssh/sshd_config


varecho=$(echocyan "Setup Firewall")
echo "$varecho"

echo "Setup Firewall"
ufw allow 22
ufw allow 80
ufw allow 443
ufw --force enable


sudo echo "tmpfs     /run/shm     tmpfs     defaults,noexec,nosuid     0     0" >> /etc/fstab

varecho=$(echocyan "Setup Fail2ban")
echo "$varecho"

echo "Installing Fail2Ban"
sudo apt-get -y install fail2ban
sudo service fail2ban restart


echo "Installing Nmap"
sudo apt-get -y install nmap
nmap -v -sT localhost >>$LOG_FILE


echo "#####################################"
echo "###### Installation Completed  ######"
echo "###### see install-server.log  ######"
echo "#####################################"
exit;
