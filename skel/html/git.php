<?php
try
{
  $payload = json_decode($_REQUEST['payload']);
}
catch(Exception $e)
{
  exit(0);
}

//log the request
file_put_contents('git.log', print_r($payload, TRUE), FILE_APPEND);

echo "GIT";
if ($payload->ref === 'refs/heads/master')
{
  // path to your site deployment script
  exec('./build.sh');
}
?>
