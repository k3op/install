#!/bin/bash
# K3op System
# Do stuff on servers

echo -e "\e[35m######################################"
echo -e "#### Install Server: k3op systems ####"
echo -e "######################################\e[39m"

if [ "$(id -u)" != "0" ]; then
   echo "Error : This script must be run as root\nEnable root user with: sudo passwd root" 1>&2
   exit 1
fi

PS3='Please enter your choice: '
options=("Download Tools" "Setup Server" "Setup Nginx" "Create a vHost" "Quit")
width=25
cols=1


# Define echo function
# Blue color
function ee_lib_echo()
{
    echo $(tput setaf 4)$@$(tput sgr0)
}
# White color
function ee_lib_echo_info()
{
    echo $(tput setaf 7)$@$(tput sgr0)
}
# Red color
function ee_lib_echo_fail()
{
    echo $(tput setaf 1)$@$(tput sgr0)
}

cd

for ((i=0;i<${#options[@]};i++)); do 
  string="$(($i+1))) ${options[$i]}"
  printf "%s" "$string"
  printf "%$(($width-${#string}))s" " "
  [[ $(((i+1)%$cols)) -eq 0 ]] && echo
done

while true; do
  echo
  read -p '? ' opt
  case $opt in
    1)
      echo -e "\e[35m######################################"
      echo -e "#### K3op Tools : Download Tools  ####"
      echo -e "######################################\e[39m"
      echo "${options[$opt-1]}"
      sudo apt install -y git

      ssh-keygen -t rsa -b 4096 -C "contact@k3op.com"
      cat ~/.ssh/id_rsa.pub

      git clone ssh://git@bitbucket.org/k3op/sysadmin-k3op.git ~/sysadmin-k3op
      cd ~/sysadmin-k3op/install/script
      chmod 755 *
      ;;

    2)
      echo -e "\e[35m######################################"
      echo -e "#### K3op Tools : Setup Server  ####"
      echo -e "######################################\e[39m"
      echo "${options[$opt-1]}"
      sh ~/sysadmin-k3op/install/script/install-server.sh
      ;;

    3)
      echo -e "\e[35m######################################"
      echo -e "#### K3op Tools : Setup Nginx ###"
      echo -e "######################################\e[39m"
      echo "${options[$opt-1]}"
      sh ~/sysadmin-k3op/install/script/install-nginx.sh
      ;;

    4)
      echo "${options[$opt-1]}"
      sh create-host.sh
      ;;

    5)
      echo "Bye bye!"
      break
      ;;
  esac
done
