FROM ubuntu:latest

MAINTAINER K3OP <contact@k3op.com>

ENV DEFAULT_APP_ENV production
ENV DEFAULT_CHOWN_APP_DIR true
ENV DEFAULT_VIRTUAL_HOST example.com
ENV DEFAULT_TIMEZONE France/Paris
ENV EMAIL contact@k3op.com

RUN apt-get update -y

# Default to UTF-8 file.encoding
ENV LANG C.UTF-8

# Install NGINX
RUN  apt-get install -y nginx
# Install PHP
RUN apt-get -y install php7.0-fpm

# LOG forward request logs to Docker log collector
RUN ln -sf /dev/stdout /var/log/nginx/access.log
RUN ln -sf /dev/stderr /var/log/nginx/error.log

RUN rm /var/www/html/index.nginx-debian.html

# COPY HTML
COPY html/* /var/www/html/

# COPY PHP
COPY skel/php.ini /etc/php/7.0/fpm/php.ini

# Configure Nginx
RUN rm /etc/nginx/sites-available/default
COPY skel/default.conf /etc/nginx/sites-available/default
COPY skel/nginx.conf /etc/nginx/nginx.conf

# Expose ports
EXPOSE 80
EXPOSE 443

# Define default command
CMD service php7.0-fpm start && nginx -g "daemon off;"
