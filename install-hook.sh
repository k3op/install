
wwwDir='/var/www/'
sourceDir='/html'
domain='jprouve-door-auction.com'

touch $wwwDir$domain$sourceDir/build.sh

sudo sh -c  "echo '
#!/bin/sh
GIT_WORK_TREE=$wwwDir$domain
export GIT_WORK_TREE
git checkout -f
git pull
' >> $wwwDir$domain$sourceDir/build.sh"
